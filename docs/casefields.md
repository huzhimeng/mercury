# 裁判文书网字段信息描述

### 字段名称含义

```Python
FIELDS: {
    'docid': '文书ID',
    'title': '案件名称',
    'court': {
        'id': '法院ID',
        'name': '法院名称',
        'nation': '法院国家',
        'province': '法院省份',
        'county': '法院区县',
        'area': '法院区域',
        'level': '法院层级',
    },
    'casetype': '案件类型',
    'proceedings': '审理程序',
    'doctype': '文书类型',
    'cn': '案号',
    'cause': '案由',
    'admins': '行政人员',
    'mends': '补正文书',
    'pubdate': '发布日期',
    'uploaddate': '上传日期',
    'closeway': '结案方式',
    'trialdate': '裁判日期',
    'legalbasis': '法律依据',
    'content': '裁判文书全文',
    'docinfo': {
        'type': '文书全文类型',
        'preamble': '文本首部段落原文',
        'baseinfo': '案件基本情况段原文',
        'adjudication': '裁判要旨段原文',
        'litigants': '诉讼参与人信息部分原文',  # ???
        'verdict': '判决结果段原文',
        'judicialrecords': '诉讼记录段原文',
        'tail': '文本尾部原文',
        'addition': '附加原文',
    },
    'litigants': '当事人',  # ???
    'lawoffice': '律所',
    'lawyers': '律师',
    'nonpublicreason': '不公开理由',
    'effectlevel': '效力层级',
    'doccontent': 'DocContent',  # 一般不存在
    'adminscope': '行政管理范围',  # 一般不存在
    'adminact': '行政行为种类',  # 一般不存在
    'prosecutor': '公诉机关',  # 一般不存在 public prosecution organ
    'relatedoc': '关联文书',
}
```

### 法律依据字段解释

```Python
legalbasis: [
    {
        'legalname': '法律名称',
        'legalbasis': # '条目'
        [
            {
                'legalspec': '法条名称',
                'legalcontent': '法条内容',
            },
        ]
    },
]
```

### 裁判文书全文字段解释

```Python
content: {
    'WBSB': '首部',
    'DSRXX': '当事人信息',
    'SSJL': '上诉记录',
    'AJJBQK': '案件基本情况',
    'CPYZ': '裁判要旨',
    'PJJG': '判决结果',
    'WBWB': '尾部',
}
```

### 关联文书字段

```Python
relatedoc: [
    {
        "docid": '文书ID',
        "doctype" : '文书类型',
        "proceedings" : '审理程序',
        "court" : '法院名称',
        "cn" : '案号',
        "closeway" : '结案方式',
        "trialdate" : '裁判日期'
    },
]
```

### 行政人员字段

```Python
admins: {
    '审判长': 'xxx',
    '法官助理': 'xxx',
    '审判员': 'xxx',
    '代理审判员': 'xxx',
    '书记员': 'xxx',
    '代理书记员': 'xxx',
    '实习书记员': 'xxx'
}
```
