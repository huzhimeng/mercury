# -*- coding: utf-8 -*-

#!/usr/bin/env python
# coding:utf-8

# import sys
# sys.path.insert(0, "/Users/ju/workspace/mercury")

import aiohttp

from mercury.utils import md5
from mercury.settings import RuoKuaiConf as Conf


class RuoKuai:

    def __init__(self, username, password, soft_id, soft_key):
        self.username = username
        self.password = md5(password)
        self.soft_id = soft_id
        self.soft_key = soft_key
        self.base_params = {
            'username': self.username,
            'password': self.password,
            'softid': self.soft_id,
            'softkey': self.soft_key,
        }
        self.headers = {
            'Connection': 'Keep-Alive',
            'Expect': '100-continue',
            'User-Agent': 'ben',
        }

    async def identify(self, im, im_type, timeout=60):
        """
        im: 图片字节
        im_type: 题目类型

        return example: {'Result': '3vpm', 'Id': '78295742-e6c1-403e-8bf5-1cc120da5982'}
        """
        params = {
            'typeid': im_type,
            'timeout': timeout,
            "image": im
        }
        params.update(self.base_params)
        async with aiohttp.ClientSession(headers=self.headers) as se:
            async with se.post('http://api.ruokuai.com/create.json',
                               data=params) as r:
                return await r.json(content_type="text/json; charset=utf-8")

    async def report_error(self, im_id):
        """
        im_id:报错题目的ID
        """
        params = {
            'id': im_id,
        }
        params.update(self.base_params)
        async with aiohttp.ClientSession(headers=self.headers) as se:
            async with se.post('http://api.ruokuai.com/reporterror.json',
                               data=params) as r:
                return await r.json(content_type="text/json; charset=utf-8")


ruokuai = RuoKuai(Conf.username, Conf.password,
                  Conf.softid, Conf.softkey)


if __name__ == '__main__':
    import asyncio
    loop = asyncio.get_event_loop()
    im = open('/Users/ju/Downloads/captcha3.jpeg', 'rb').read()
    print(loop.run_until_complete(ruokuai.identify(im, Conf.FOUR_DIGIT_LETTER)))


