"""获取代理IP"""

from mercury.settings import DEPLOYTYPE, PROXIES


def get_proxy():
    """获取代理IP
    如果爬虫机器在国内阿里云上, 则访问代理服务器的内网IP
    如果爬虫机器在海外或是开发测试环境下, 则访问代理服务器的公网IP
    """
    return PROXIES[DEPLOYTYPE]
