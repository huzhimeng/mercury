# -*- coding: utf-8 -*-

import re
import os
import uuid
import yaml
import random
from datetime import datetime, timezone, timedelta

import execjs
import demjson
from bs4 import BeautifulSoup

from mercury.utils import md5
from mercury.libs import s3, ruokuai
from mercury.libs.mongo import mgdb
from mercury.libs.proxy import get_proxy
from mercury.libs.useragent import random_ua
from mercury.spiders import BaseSpider
from mercury.models import Case
from mercury.downloader.http import AsyncHttpDownloader
from mercury.settings import logger, DIR_PROJECT, RuoKuaiConf
from ._ws_util import parse_content_js


url_base = "http://wenshu.court.gov.cn"
url_cookie = url_base + "/list/list/"
url_case = url_base + "/content/content?DocID={docid}"
url_captcha_img = url_base + "/waf_captcha/?{}"
url_captcha_api = url_base + "/waf_verify.htm?captcha={captcha}"
url_ackcode = url_base + "/ValiCode/GetCode"
url_indexapi = url_base + "/List/ListContent"
url_relatedocs = url_base + "/Content/GetDocRelationAndFuse"
url_detailapi = url_base + \
    "/CreateContentJS/CreateContentJS.aspx?DocID={docid}"

re_chinese = re.compile('[^\u4e00-\u9fa5]')
re_index = re.compile("Index=(?P<Index>(\d+))")
re_wenshu = re.compile("""<a type='dir' name='\w+'></a>""")
re_tag = re.compile("""<a type='dir' name='(?P<name>\w+)'></a>""")
re_update = re.compile("Date\((?P<timestamp>\d+)\)")


FIELDS = {
    'docid': '文书ID',
    'title': '案件名称',
    'court': {
        'id': '法院ID',
        'name': '法院名称',
        'nation': '法院国家',
        'province': '法院省份',
        'county': '法院区县',
        'area': '法院区域',
        'level': '法院层级',
    },
    'casetype': '案件类型',
    'proceedings': '审理程序',
    'doctype': '文书类型',
    'cn': '案号',
    'cause': '案由',
    'admins': '行政人员',
    'mends': '补正文书',
    'pubdate': '发布日期',
    'uploaddate': '上传日期',
    'closeway': '结案方式',
    'trialdate': '裁判日期',
    'legalbasis': '法律依据',
    'content': '裁判文书全文',
    'docinfo': {
        'type': '文书全文类型',
        'preamble': '文本首部段落原文',
        'baseinfo': '案件基本情况段原文',
        'adjudication': '裁判要旨段原文',
        'litigants': '诉讼参与人信息部分原文',  # ???
        'verdict': '判决结果段原文',
        'judicialrecords': '诉讼记录段原文',
        'tail': '文本尾部原文',
        'addition': '附加原文',
    },
    'litigants': '当事人',  # ???
    'lawoffice': '律所',
    'lawyers': '律师',
    'nonpublicreason': '不公开理由',
    'effectlevel': '效力层级',
    'doccontent': 'DocContent',  # 一般不存在
    'adminscope': '行政管理范围',  # 一般不存在
    'adminact': '行政行为种类',  # 一般不存在
    'prosecutor': '公诉机关',  # 一般不存在 public prosecution organ
    'relatedoc': '关联文书',
}


def _load_captcha():
    captcha_path = os.path.join(
        DIR_PROJECT, 'mercury/spiders/wscourt/captcha.yml')
    with open(captcha_path, 'r', encoding='utf-8') as f:
        return yaml.load(f)


def _compile_jscode():
    """ compile js code
    """
    js_path = os.path.join(DIR_PROJECT, 'mercury/spiders/wscourt/case.js')
    with open(js_path, mode='r', encoding='utf-8') as f:
        js_code = f.read()
    js_compiler = execjs.compile(js_code)
    return js_compiler


class Spider(BaseSpider):
    """
    裁判文书爬虫
    """

    name = "wscourt"

    Model = Case

    captcha_identifer = _load_captcha()
    js_executor = _compile_jscode()

    # 案件类型
    casetypes_mapping = {"1": "刑事案件", "2": "民事案件",
                         "3": "行政案件", "4": "赔偿案件", "5": "执行案件"}
    # 审判人员类别
    admin_types = ('审判长', '法官助理', '人民陪审员', '陪审员',  '代理审判员',
                   '审判员', '实习书记员', '代理书记员', '代书记员', '书记员')
    # 无效的页面字符串特征
    invalid_words = ('remind', 'HtmlNotExist', 'VisitRemind',
                     '开启JavaScript', '刷新该页', '文书不存在')

    def __init__(self, job):
        super().__init__(job)

    def setup_configs(self):
        configs = {
            "post_data": None,
            "headers": {
                "User-Agent": random_ua(),
            },
            "cookies": {},
            "sleep_range_before_request": (60, 80),
            "uniqid": "docid",
            "save_html": False,
            "proxy_enable": True
        }
        return configs

    async def before_request(self):
        self.dl = AsyncHttpDownloader(
            timeout=180,  proxy=get_proxy(),
            headers=self.configs.get('headers'))
        if self.job.is_seed:
            # get cookie value vjkl5 and key vl5x
            await self.dl.fetch(url_cookie, auto_close=False, allow_redirects=False)
            if self.dl.cookies.get('vjkl5'):
                vjkl5 = self.dl.cookies.get("vjkl5").value
                vl5x = self.js_executor.call("getKey", vjkl5)
                guid = self.gen_code()
                ackcode, _ = await self.dl.fetch(
                    url_ackcode, method='post',
                    ctype='formdata', data={'guid': guid},
                    auto_close=False, allow_redirects=False)
                if vl5x and ackcode:
                    self.job.payload['vl5x'] = vl5x
                    self.job.payload['number'] = ackcode
                    self.job.payload['guid'] = guid
                    self.job.method = 'post'
                    self.job.ctype = 'formdata'
                    self.job.rtype = 'json'
                else:
                    await self.dl.close()
                    return False
            else:
                await self.dl.close()
                return False
        # 默认返回True
        return True

    async def handle_index(self, page):
        """
        handle list page
        """
        detail_infos, next_info = [], {}
        # 返回的json格式是字符串的字符串, 所以需要再次decode
        data = self.decode_json(page)
        if data and isinstance(data, list) and len(data) > 1:
            for doc in data[1:]:
                detail_infos.append({'url': url_detailapi.format(
                    docid=doc['文书ID']), 'auto_close': False})

            if detail_infos:
                self.job.payload['Index'] += 1
                urlsnum = len(detail_infos)
                # 获取的文书ID数量小于指定的每页数量, 将指定的每页数量调整为5的倍数
                self.job.payload['Page'] = urlsnum if urlsnum % 5 == 0 else 5
                next_info = {'url': self.job.url,
                             'method': 'post',
                             'ctype': 'formdata',
                             'rtype': 'json',
                             'auto_close': True,
                             'payload': self.job.payload}
        # 打印每个查询条件获取的详情页面urls的数量
        logger.info('[Spider]<{}> urls num: {}, payload: {}'.format(
            self.name, len(detail_infos), self.job.payload))

        return detail_infos, next_info

    async def handle_detail(self, page, encoding, extra_data=None):
        """
        handle detail page
        """
        retval = None

        # 下载失败或者触发反爬
        if any(map(lambda w: w in page, self.invalid_words)):
            await self.dl.close()
            return retval

        # 页面出现验证码
        if "访问验证" in page:
            page = await self.handle_captcha()
            # 验证码处理失败
            if not page:
                await self.dl.close()
                return retval

        try:
            # 案件信息, 案件相关法律信息, 文书主体信息
            case_info, relate_info, html_info = parse_content_js(page)
        except Exception as e:
            logger.error(
                '[Spider]<{}> parse_content_js failed, page: {}'.format(self.name, page))
        else:
            payload = {
                'docid': case_info['文书ID'],
                'court': case_info['法院名称'],
                'caseNumber': case_info['案号'],
                'caseType': case_info['案件类型']
            }
            # 关联文书
            relate_doc = None
            relate_src, _ = await self.dl.fetch(
                url_relatedocs, method='post', ctype='formdata',
                data=payload, rtype='json', auto_close=False)
            # 返回的json格式是字符串的字符串, 所以需要再次decode
            relatedocs = self.decode_json(relate_src)
            # 关联文书获取失败, 重新发起一次请求
            if not relatedocs:
                relate_src, _ = await self.dl.fetch(
                    url_relatedocs, method='post',
                    ctype='formdata', data=payload, rtype='json')
                relatedocs = self.decode_json(relate_src)
            if relatedocs:
                relate_doc = self._extract_relatedoc(relatedocs)

            retval = self._extract_detail(
                case_info, relate_info, html_info, relate_doc)
        finally:
            await self.dl.close()
            return retval

    async def handle_captcha(self):
        """
        handle captcha image
        """
        logger.info('[Spider]<{}> handle captcha'.format(self.name))
        page = None
        img_url = url_captcha_img.format(str(random.random()))
        img, _ = await self.dl.fetch(img_url, rtype='read', auto_close=False)
        if not img:
            logger.info(
                '[Spider]<{}> get captcha image failed'.format(self.name))
            return page

        captcha = self.captcha_identifer.get(md5(img))
        # 图片验证码不在哈希表中
        if not captcha:
            # 使用若快提供的验证码接口识别
            result = await ruokuai.identify(img, RuoKuaiConf.FOUR_DIGIT_LETTER)
            # 若快识别错误
            if result.get('Error'):
                await ruokuai.report_error(result['id'])
                logger.error('[Spider]<{}> ruokuai error: {}'.format(
                    self.name, result['Error']))
            # 若快识别成功
            else:
                captcha = result.get('Result')
                # 将新的验证码图片存到s3上
                await s3.upload_file(img, 'captcha/court_gov_cn/{guid}_{captcha}.jpg'.format(
                    guid=result['id'], captcha=captcha))
                logger.info(
                    '[Spider]<{}> new captcha: {}'.format(self.name, captcha))

        if captcha:
            # 提交验证码识别结果
            verify_url = url_captcha_api.format(captcha=captcha)
            page, _ = await self.dl.fetch(verify_url, auto_close=False)
        logger.info('[Spider]<{}> handle captcha {}'.format(
            self.name, 'succeed' if page and self.dl.url == self.job.url else 'failed'))
        return page

    def _fields_convert(self, case):
        """convert case keys from chinese to english"""
        url = url_case.format(docid=case["文书ID"])
        retval = {
            'url': url,
            'sid': md5(url),
            'getway': self.name,
        }
        # 中英字段转换
        retval.update({k: case.get(v if isinstance(v, str) else k)
                       for (k, v) in FIELDS.items()})
        return retval

    def _extract_detail(self, meta_info, related_info, html_info, relate_doc=None):
        """
        extract detail page info
        """

        meta_info["案件类型"] = self._extract_casetype(meta_info)
        meta_info["上传日期"] = self._update_convert(meta_info["上传日期"])
        meta_info["审理程序"] = meta_info.pop("审判程序", None)

        # process relate info
        relate_info = {}
        for item in related_info["RelateInfo"]:
            relate_info[item["name"]] = item["value"]
        # appellor
        if "当事人" in relate_info:
            relate_info["当事人"] = list(
                map(str.strip, relate_info["当事人"].split(",")))
        else:
            relate_info["当事人"] = []

        # process legalbase info
        legal_base = related_info.pop("LegalBase")
        for dicitem in legal_base:
            dicitem["legalname"] = dicitem.pop("法规名称", None)
            dicitem['legalbasis'] = dicitem.pop('Items', [])
            for item in dicitem["legalbasis"]:
                item["legalspec"] = item.pop("法条名称", None)
                item["legalcontent"] = item.pop("法条内容", None)

        # process doc info
        html_info["Html"] = html_info["Html"].replace("01lydyh01", "\'")
        html_info["Html"] = self._extract_html(html_info["Html"])
        if html_info["Html"] and "WBWB" in html_info["Html"][-1]:
            wbwb = html_info["Html"][-1]["WBWB"]
            admins = self._extract_judgerinfo(wbwb)
        else:
            admins = {}

        case = meta_info
        case["发布日期"] = html_info.pop("PubDate", None)
        case["案件标题"] = html_info.pop("Title", None)
        case['行政人员'] = admins
        case["裁判文书全文"] = html_info.pop("Html")
        case["法律依据"] = legal_base
        case['关联文书'] = relate_doc
        case.update(relate_info)

        case['court'] = {
            'id': case.pop('法院ID', None),
            'name': case.pop('法院名称', None),
            'nation': case.pop('法院国家', None),
            'province': case.pop('法院省份', None),
            'county': case.pop('法院区县', None),
            'area': case.pop('法院区域', None),
            'level': case.pop('法院层级', None)
        }
        case['docinfo'] = {
            'type': case.pop('文书全文类型', None),
            'preamble': case.pop('文本首部段落原文', None),
            'baseinfo': case.pop('案件基本情况段原文', None),
            'adjudication': case.pop('裁判要旨段原文', None),
            'litigants': case.pop('诉讼参与人信息部分原文', None),  # ???
            'verdict': case.pop('判决结果段原文', None),
            'judicialrecords': case.pop('诉讼记录段原文', None),
            'tail': case.pop('文本尾部原文', None),
            'addition': case.pop('附加原文', None)
        }
        return self._fields_convert(case)

    def _extract_casetype(self, case):
        # get case type chinese discription, it's maybe a string of the number
        ty1 = case.get('案件类型')
        # if string number, then, get chinese edition by itself
        ty2 = self.casetypes_mapping.get(ty1)
        return ty2 or ty1

    @staticmethod
    def _extract_relatedoc(relate_doc):
        # 关联文书字段转换
        ret = []
        for item in relate_doc['RelateFile']['RelateFile']:
            doc = {}
            doc['docid'] = item.pop('文书ID')
            doc['doctype'] = item.pop('Type')
            doc['proceedings'] = item.pop('审判程序')
            doc['court'] = item.pop('审理法院')
            doc['cn'] = item.pop('案号')
            doc['closeway'] = item.pop('结案方式')
            doc['trialdate'] = item.pop('裁判日期')
            ret.append(doc)
        return ret

    @staticmethod
    def _update_convert(date_str):
        digit_ret = re_update.search(date_str)
        if digit_ret:
            digit = int(digit_ret.group("timestamp")) / 1000
            tz_utc_8 = timezone(timedelta(hours=8))
            t = datetime.fromtimestamp(digit, tz=tz_utc_8)
            return t.astimezone(tz=timezone.utc)
        return None

    @staticmethod
    def _extract_judgerinfo(judge_str):
        admins = {}
        for line in judge_str.splitlines():
            line = re_chinese.sub('', line)
            for field in Spider.admin_types:
                if field in line:
                    admins[field] = line.replace(field, '')
                    break
        return admins

    @staticmethod
    def _extract_html(html):
        docs = []
        htmls = re_tag.split(html)[1:]
        for i in range(0, len(htmls), 2):
            soup = BeautifulSoup(htmls[i + 1], "lxml")
            item = {htmls[i]: soup.get_text("\n", strip=True)}
            docs.append(item)
        return docs

    @staticmethod
    def gen_code():
        uid = str(uuid.uuid4())
        return uid[:18] + uid[19:]

    @staticmethod
    def decode_json(input):
        if not isinstance(input, str):
            return input
        retval = None
        try:
            retval = demjson.decode(input)
        except (demjson.JSONDecodeError, Exception) as e:
            logger.error(
                '[Spider]<{}> decode_json failed, input: {}'.format(self.name, input))
        finally:
            return retval
