# -*- coding: utf-8 -*-

"""
商标数据的存储模型
"""
from mercury.libs.mongo import mgdb
from mercury.utils import md5, timestamp
from ._base import Model as BaseModel


class Trademark(BaseModel):

    _db = mgdb.trademark
    _uniqid = "guid"

    def __init__(self, url, urlmd5, **kwargs):
        self._id = kwargs.pop("_id", None)
        self.url = url                                                # 商标URL
        self.urlmd5 = urlmd5,                                         # URLMD5
        self.create_time = timestamp()
        self.status = kwargs.pop("status", None)                      # 商标状态
        self.notice_first_trial = kwargs.pop(
            "notice_first_trial", None)                               # 初审公告
        self.reg_code = kwargs.pop("reg_code", None)                  # 注册码
        self.inter_class = kwargs.pop("inter_class", None)            # 国际分类
        self.name_zh = kwargs.pop("name_zh", None)                    # 商标中文名称
        self.name_en = kwargs.pop("name_en", None)                    # 英文名称
        self.date_apply = kwargs.pop("date_apply", None)              # 申请日期
        self.date_first_trial = kwargs.pop("date_first_trial", None)  # 初审日期
        self.date_reg = kwargs.pop("date_reg", None)                  # 注册日期
        self.date_exp = kwargs.pop("date_exp", None)                  # 截止日期
        self.reg_person = kwargs.pop("reg_person", None)              # 注册人
        self.reg_addr = kwargs.pop("reg_addr", None)                  # 注册地址
        self.agency = kwargs.pop("agency", None)                      # 代理组织
        self.services = kwargs.pop("services", None)                  # 使用商品
        self.features = kwargs.pop("features", [])                    # 图片特征
        self.url_image = kwargs.pop("url_image", None)                # 商标链接
        self.reg_process = kwargs.pop("reg_process", None)            # 注册流程
        self.getway = kwargs.pop("getway", None)                      # 获取途径
        # seed id
        self.sid = kwargs.pop("sid", None)
        # url中的查询参数guid
        self.guid = kwargs.pop("guid", None)
