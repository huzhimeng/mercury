# -*- coding: utf-8 -*-

"""种子管理API
"""

from sanic import Blueprint
from sanic.response import json
from mercury.models.seed import Seed

bp = Blueprint("seeds")


@bp.get("/seeds")
async def get_all(request):
    """获取全部seeds，默认分页，支持URL查询参数

    page: number
    per_page: number
    sort: create, update
    direction: asc, desc

    getway: baidunews...
    status: ok, stopped

    返回元素范围 [(page-1)*per_page, page*per_page)

    {
        "total": 1000,
        "page": 1,
        "per_page": 20,
        "rows":[
            {"id":1, "url":2},
            {"id":2, "url":2}
        ]
    }
    """
    return json({"test": "get_all"})


@bp.get("/seeds/<urlmd5>")
async def get_one(request, urlmd5):
    """根据指定id号返回种子信息

    {
        "url": "http://news.baidu.com/123",
        "urlmd5": "0E45232",
        "getway": "baidunews",
        "frequency": 3600,
        "crawled_times": 2,
        "status": "stoppd"
    }
    """
    return json({"test": "get_one"})


@bp.post("/seeds")
async def create_one(request):
    """根据传递的信息创建和保存一个seed到数据库中，并返回创建后的信息
    注意，每一次post，都将创建一个新的对象

    {
        "url": "http://news.baidu.com/124",
        "urlmd5": "0E45243",
        "getway": "baidunews",
        "frequency": 3600,
        "crawled_times": 2,
        "status": "ok"
    }
    """

    retval = {"status": 1, "message":"Create successfully.", "data": None}

    json_data = request.json
    data = await Seed(**json_data).save()
    if isinstance(data, Exception):
        retval["message"] = "Create failed, {0}.".format(data)
    else:
        retval["data"] = data
    return json(retval)


@bp.put("/seeds/<urlmd5>")
async def replace_one(request, urlmd5):
    """把指定 urlmd5 的seed信息替换为传入的样子内容，返回替换后的信息
    注意，是整体替换，而非部分更新，需提供seed的所有字段
    """
    return json({"test": "replace_one"})


@bp.patch("/seeds/<urlmd5>")
async def update_one(request, urlmd5):
    """更新指定urlmd5的seed，如修改status字段，应用此方法，返回更新后的信息
    """
    return json({"test": "update_one"})


@bp.delete("/seeds/<urlmd5>")
async def delete_one(request, urlmd5):
    """根据指定 urlmd5 删除 seed，返回删除操作是否成功
    """
    return json({"test": "delete_one"})
